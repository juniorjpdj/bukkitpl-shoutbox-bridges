#!/usr/bin/python
# -*- coding: utf8 -*-
import sys
import shoutApi
from datetime import datetime
from threading import Thread
from time import sleep

import xmpp


xmppLogin = 'JID BOTA'
xmppPass = 'HASLO BOTA'
xmppStatus = 'OPIS BOTA NA XMPP'
chaturl = "http://bukkit.pl/shoutbox"
historysize = 20

version = '1.5'
logged = {}
commands = {}
aliases = {}
spectators = set()
quit = False
xmppJID = xmpp.JID(xmppLogin)
xmppUser = xmppJID.getNode()
xmppServer = xmppJID.getDomain()
xmppRes = xmppJID.getResource()

if sys.platform == "win32":
    encoding = sys.stdout.encoding
else:
    encoding = "utf8"


class asyncRun(Thread):
    def __init__(self, what, *args):
        Thread.__init__(self)
        self.what = what
        self.args = args
        self.start()

    def run(self):
        self.what(*self.args)


def showTime(time=datetime.now()):
    return time.strftime('[%H:%M:%S]')


def printP(msg):
    print(unicode(msg).encode(encoding))


def registerCmd(cmd, handler, info, inform=True):
    commands[cmd] = (handler, info, inform)


def aliasCmd(cmd1, cmd2):
    aliases[cmd1] = cmd2


def XMPPsend(u, msg, retry=3):
    err = 0
    while err < retry:
        try:
            xmppConn.send(xmpp.Message(u, msg, 'chat'))
            break
        except:
            err += 1
            sleep(0.1)
    if err == retry:
        printP(u'[XMPP] {t} [Error] Nie udało się wysłać wiadomości: {m}'.format(t=showTime(), m=msg))


def XMPPsendAll(msg):
    for u in logged.keys():
        XMPPsend(u, msg)
    for u in spectators:
        XMPPsend(u, msg)


def sendChatHistory(user):
    XMPPsend(user, u'[INFO] Wyświetlanie ostatnich {c} wiadomości'.format(c=unicode(historysize)))
    for m in Reciever.chatHistory.values()[-historysize:]:
        XMPPsend(user, u"{} {}: {}".format(showTime(m.time), m.author.nickname, m.text))


def onXMPPmsg(xmppC, msg):
    text = msg.getBody()
    user = unicode(msg.getFrom())
    if user.find('/') > 0:
        user = user[:user.find('/')]

    if not xmppRoster.getItem(user):
        printP(u'[XMPP] {t} [INFO] {u} został dodany do znajomych'.format(t=showTime(), u=user))
        xmppRoster.setItem(user)
        xmppRoster.Subscribe(user)
        xmppRoster.Authorize(user)

    if msg.getType() == 'chat' and text:
        if text.find('/') == 0:
            args = text.split(' ')
            cmd = args[0][1:]
            cmdlabel = cmd
            args = args[1:]
            if cmd in aliases:
                cmd = aliases[cmd]

            if cmd in commands:
                if commands[cmd][2]:
                    printP(u'[XMPP] {t} [INFO] {u} wykonał komendę {m}'.format(t=showTime(), u=user, m=text))
                asyncRun(commands[cmd][0], user, cmdlabel, args)
            else:
                printP(
                    u'[XMPP] {t} [INFO] {u} próbował wykonać nieznaną komendę {m}'.format(t=showTime(), u=user, m=text))
                XMPPsend(user, u'[INFO] Nie ma takiej komendy ;c')
        else:
            printP(u'[XMPP] {t} {u}: {m}'.format(t=showTime(), u=user, m=text))
            if user in logged:
                asyncRun(logged[user].send, unicode(text).encode('utf8'))
            else:
                XMPPsend(user,
                         u'[INFO] Przed wysłaniem wiadomości zaloguj się do czatu za pomocą komendy "/login", więcej komend pod komendą "/help"!')


def onXMPPdisconnect():
    printP(u'[XMPP] {t} [INFO] Rozłączono'.format(t=showTime()))
    while not xmppConn.isConnected():
        try:
            xmppConn.reconnectAndReauth()
        except Exception:
            pass
    sleep(1)
    printP(u'[XMPP] {t} [INFO] Połączono ponownie'.format(t=showTime()))
    xmppConn.RegisterHandler('message', onXMPPmsg)
    xmppConn.RegisterDisconnectHandler(onXMPPdisconnect)
    xmppConn.send(xmpp.Presence(show='chat', status=xmppStatus))


def onShoutJoin(user):
    gen = u'{n} dołączył do czatu'.format(n=user.nickname)
    printP(u'[SHOUT] {t} {g}'.format(t=showTime(), g=gen))
    XMPPsendAll(gen)


def onShoutLeave(user):
    gen = u'{n} opuścił czat'.format(n=user.nickname)
    printP(u'[SHOUT] {t} {g}'.format(t=showTime(), g=gen))
    XMPPsendAll(gen)


def onShoutMsg(msg, new):
    gen = u'{u}: {m}'.format(u=msg.author.nickname, m=msg.text)
    if new:
        printP(u'[SHOUT] {t} {g}'.format(t=showTime(msg.time), g=gen))
        XMPPsendAll(gen)


def helpCmd(user, cmd, args):
    XMPPsend(user, u"[INFO] Lista dostępnych komend:")
    for cmd in commands.keys():
        XMPPsend(user, u'[INFO] /{c} - {i}'.format(c=cmd, i=commands[cmd][1]))


def loginCmd(user, cmd, args):
    global spectators
    global logged
    if len(args) == 2:
        printP(u'[XMPP] {t} [INFO] {u} próbuje zalogować się jako {l}'.format(t=showTime(), u=user, l=args[0]))
        if user not in spectators:
            if user not in logged:
                User = shoutApi.ChatSender(chaturl)
                try:
                    User.login(args[0], args[1])
                    if User.isLoggedIn():
                        logged[user] = User
                        XMPPsend(user, u'[INFO] Zalogowano pomyślnie!')
                        printP(u'[XMPP] {t} [INFO] {u} zalogował się pomyślnie'.format(t=showTime(), u=user))
                        sendChatHistory(user)
                    else:
                        XMPPsend(user, u'[INFO] Błąd logowania, spróbuj ponownie')
                        printP(u'[XMPP] {t} [INFO] {u} próbował się zalogować - nie wyszło mu ;c'.format(t=showTime(), u=user))
                except Exception as e:
                    XMPPsend(user, u'[INFO] Błąd logowania, spróbuj ponownie')
                    printP(
                        u'[XMPP] {t} [INFO] {u} próbował się zalogować - nie wyszło mu ;c'.format(t=showTime(), u=user))
                    printP(u'[Error] {e}'.format(e=unicode(e)))
            else:
                XMPPsend(user, u'[INFO] Jesteś już zalogowany, najpierw się wyloguj!')
                printP(u'[XMPP] {t} [INFO] {u} chciał się zalogować będąc już zalogowanym'.format(t=showTime(), u=user))
        else:
            XMPPsend(user, u'[INFO] Najpierw wyloguj sie ze spektatora')
            printP(u'[XMPP] {t} [INFO] {u} próbował zalogować się będąc spektem'.format(t=showTime(), u=user))
    else:
        printP(u'[XMPP] {t} [INFO] {u} nie zna składni polecenia /{c} :D'.format(t=showTime(), u=user, c=cmd))
        XMPPsend(user, u'[INFO] Błędne użycie komendy /{c}'.format(c=cmd))
        XMPPsend(user, u'[INFO] Prawidłowe użycie: "/{c} NICK HASŁO"'.format(c=cmd))


def specCmd(user, cmd, args):
    global spectators
    global logged
    if user not in logged:
        if user not in spectators:
            spectators.add(user)
            XMPPsend(user, u'[INFO] Pomyślnie dołączono do spektatorów!')
            sendChatHistory(user)
        else:
            XMPPsend(user, u'[INFO] Jesteś już spektatorem, najpierw się wyloguj!')
    else:
        XMPPsend(user, u'[INFO] Jesteś zalogowany, najpierw się wyloguj')


def logoutCmd(user, cmd, args):
    global spectators
    global logged
    if user in logged:
        logged[user].logout()
        del (logged[user])
        XMPPsend(user, u'[INFO] Wylogowano!')
    elif user in spectators:
        spectators.remove(user)
        XMPPsend(user, u'[INFO] Wylogowano ze spektatora!')
    else:
        XMPPsend(user, u'[INFO] Przed wylogowaniem, wypadałoby się zalogować ;)')


def listCmd(user, cmd, args):
    usrlist = u''
    for usr in Reciever.userList:
        usrlist += u' ' + usr.nickname
    XMPPsend(user, u'[INFO] Lista osób uczestniczących w czacie:{l}'.format(l=usrlist))


def verCmd(user, cmd, args):
    XMPPsend(user,
             u'[INFO] Korzystasz z mostka shoutbox bukkit.pl <--> XMPP by JuniorJPDJ w wersji {v}'.format(v=version))


def Quit():
    global quit
    quit = True
    printP(u'[INFO] Trwa wyłączanie mostka')
    xmppConn.send(xmpp.Presence(typ='unavailable', show='unavailable', status=''))
    Reciever.stop()
    for u in logged.values():
        u.logout()


def StepOn(xmppConn):
    if quit:
        return 0
    else:
        try:
            xmppConn.Process(1)
        except KeyboardInterrupt:
            Quit()
            return 0
        except IOError:
            pass
        return 1


xmppConn = xmpp.Client(xmppServer, debug=[])
if not xmppConn.connect():
    printP(u'[XMPP] [Error] Nie można połączyć z serwerem {s}!'.format(s=xmppServer))
    sys.exit(1)

Reciever = shoutApi.ChatReciever(chaturl)
Reciever.regHandler('msg', onShoutMsg)
Reciever.regHandler('join', onShoutJoin)
Reciever.regHandler('leave', onShoutLeave)

registerCmd('help', helpCmd, u'wyświetla ten komunikat')
registerCmd('login', loginCmd, u'pozwala się zalogować', False)
registerCmd('spectate', specCmd, u'pozwala ogladać czat bez logowania się')
registerCmd('logout', logoutCmd, u'pozwala się wylogować lub przestać obserwować czat')
registerCmd('list', listCmd, u'wyświetla listę graczy')
registerCmd('version', verCmd, u'wyświetla wersję programu')
aliasCmd('l', 'login')
aliasCmd('lo', 'logout')
aliasCmd('quit', 'logout')
aliasCmd('q', 'logout')
aliasCmd('s', 'spectate')
aliasCmd('spec', 'spectate')
aliasCmd('ls', 'list')
aliasCmd('h', 'help')
aliasCmd('ver', 'version')

if not xmppConn.auth(xmppUser, xmppPass, xmppRes):
    printP(u'[XMPP] [Error] Nie można zalogować się na serwer {s} - sprawdź login i hasło.'.format(s=xmppServer))
    sys.exit(1)

xmppConn.sendInitPresence()
xmppRoster = xmppConn.getRoster()
xmppConn.RegisterHandler('message', onXMPPmsg)
xmppConn.RegisterDisconnectHandler(onXMPPdisconnect)
xmppConn.send(xmpp.Presence(show='chat', status=xmppStatus))

Reciever.start()

printP(u'[INFO] Mostek uruchomiony!')

while StepOn(xmppConn): pass